Goal
====

Those are gitlab CI templates to include in you projects

How to add a security check task
================================

The `security-ci.yml` is a gitlab CI include file to check for **security** issues in your dependencies.

- Add those lines to the top of your `.gitlab-ci.yml` file:
  ```yaml
  include:
    - remote: 'https://gitlab.com/animeka/ci-templates/raw/master/security-ci.yml'
  stages:
    - test
    - ...
  security:
    extends: .security
  ```

Usage
-----

Job is automatically run on each branch commit/update.

How to add a secret check task
==============================

The `secret-ci.yml` is a gitlab CI include file to check for **secret leaks** in your repository.

- Add those lines to the top of your `.gitlab-ci.yml` file:
  ```yaml
  include:
    - remote: 'https://gitlab.com/animeka/ci-templates/raw/master/secret-ci.yml'
  stages:
    - test
    - ...
  secret:
    extends: .secret
  ```
- Define an optional `leaks.toml` file to configure your exception or new secret rules.  
  See https://gitlab.com/animeka/git-secret-analyzer

Usage
-----

Job is automatically run on each branch commit/update.

How to add a linter check task
==============================

The `linter-ci.yml` is a gitlab CI include file to run a **linter** in your repository.

Multiple linter exists, for python or php.

- Add those lines to the top of your `.gitlab-ci.yml` file:
  ```yaml
  include:
    - remote: 'https://gitlab.com/animeka/ci-templates/raw/master/linter-ci.yml'
  stages:
    - test
    - ...
  linter:
    extends: .linter_py310
  ```

Notes
-----

Existing linters:
- `.linter_php5` (php 5)
- `.linter_php7` (php 7)
- `.linter_php8` (php 8)
- `.linter_py38` (python 3.8)
- `.linter_py39` (python 3.9)
- `.linter_py310` (python 3.10)
- `.linter_py311` (python 3.11)

Php expects `./run_linter` script else if will run `./vendor/bin/phplint`.

Python expects `./run_linter` script else if will run `poetry run flake8` or `pipenv run flake8`.

Usage
-----

Job is automatically run on each branch commit/update.

How to add a python type checker task
=====================================

The `type-checker-ci.yml` is a gitlab CI include file to run a python **type checker** in your repository.

- Add those lines to the top of your `.gitlab-ci.yml` file:
  ```yaml
  include:
    - remote: 'https://gitlab.com/animeka/ci-templates/raw/master/type-checker-ci.yml'
  stages:
    - test
    - ...
  linter:
    extends: .type_checker_py310
  ```

Notes
-----

Existing linters:
- `.type_checker_py38` (python 3.8)
- `.type_checker_py39` (python 3.9)
- `.type_checker_py310` (python 3.10)
- `.type_checker_py311` (python 3.11)

Python will run `poetry run mypy .` or `pipenv run mypy .`.

Usage
-----

Job is automatically run on each branch commit/update.

How to tag you project with a task
==================================

The `tag-ci.yml` is a gitlab CI include file to **tag** your project based on **semantic versionning**.

- Add those lines to the top of your `.gitlab-ci.yml` file:
  ```yaml
  include:
    - remote: 'https://gitlab.com/animeka/ci-templates/raw/master/tag-ci.yml'
  stages:
    - ...
    - release
  tag:
    extends: .tag_python
    needs:
      - task1
      - task2
      - ...
  ```
  There is also a `.tag_php` job to extend.
- Configure you project variable to have a `DEPLOY_KEY_TO_TAG` file variable which should be a **deploy key** with **write** permissions on the repository.

Notes
-----

The python one expects the version to be in `[tool.poetry]` section of `pyproject.toml` file or `[metadata]` section of `setup.cfg` file.

The php one expects the version to be in the `composer.json` file.

The `CHANGELOG.md` file will be updated if the version last section starts with `## Next`.

You can also provide `*.md` files in one of `Fixed`, `Changed`, `Added`, `Removed` directories in the `_CHANGELOGS` root directory

Usage
-----

Run your manual job and define the `VERSION_UPDATE` variable to either `major`, `minor` or `patch`.

Alternative
-----------

Use this config:
```yaml
.tag_base:
  extends: .tag_python
  needs:
    - task1
    - task2
    - ...
tag:major:
  extends: .tag_base
tag:minor:
  extends: .tag_base
tag:patch:
  extends: .tag_base
```

Then you won’t need to define any `VERSION_UPDATE` variable but just pick the right job. The `:location` suffix will indicates the type of version update.

How to publish your project docker image
========================================

The `publish-ci.yml` is a gitlab CI include file to **build**, **test**, and **publish** a docker image for your project in the gitlab registry.

- Add those lines to the top of your `.gitlab-ci.yml` file:
  ```yaml
  include:
    - remote: 'https://gitlab.com/animeka/ci-templates/raw/master/publish-ci.yml'
  stages:
    - ...
    - publish
  publish:
    extends: .publish
  image_security:
    extends: .image_security
    needs:
      - publish
  ```
- Configure your project to enable the container regitry.
- Create a `docker` directory at your project’s root and either:
  - Create a `Dockerfile` in this directory. The context will be the project’s root. The version to build (without any leading `v`) will be available as `VER` build arg.
  - Create a `docker-build` runnable script. It will take the full image name as first argument (`registry/project:version` format) and any other docker build arguments.

Notes
-----

As the docker context for the `Dockerfile` is the project’s root, be careful to reference any files located in `docker` directory as `COPY ./docker/that_file /dest/`.

Usage
-----

Any **tag** on your repository will create and publish a docker image with the version matching your tag.

Each image is also scanned for security issues with *Aquasec* **Trivy**.

This task is allowed to fail, so it’s up to you to decide if you still want to use that image or not.

How to deploy your product
==========================

The `deploy-ci.yml` is a gitlab CI include file to **deploy** a product (with one or more project components) to a **capp** docker server.

- Add those lines to the top of your `.gitlab-ci.yml` file:
  ```yaml
  include:
    - remote: 'https://gitlab.com/animeka/ci-templates/raw/master/deploy-ci.yml'
  stages:
    - ...
    - deploy
  deploy:
    extends: .deploy
  ```
- Create the necessary files to deploy:
  - `metadata` capp file
  - `components` file with one component name per line
  - `context` directory with at least the `docker-compose.yml` file
  - `confs/ENV/context` directory for each ENV environment you want to deploy.  
    Ensure to [encrypt your secrets](https://gitlab.com/animeka/ci-templates/raw/master/secret/README.md) there if you have some.
- Add some git hooks to your repository by running this:
  ```sh
  url="https://gitlab.com/animeka/ci-templates/-/raw/master/add-deploy-git-hooks"; curl -s "$url"; echo "Continue? "; read -r ANS; [ "$ANS" = "y" ] && curl -s "$url" | bash
  ```
  Feel free to review the script before execute it blindly.
- Configure you project variable to have a `APP_SERVER_HOST` variable which describe your **capp** server host.
- Configure you project variable to have a `APP_DEPLOY_KEY_FILE` file variable which should be a **private openssh key** to log into `APP_SERVER_HOST` server.  
  The user should have the right to upload a `DCA` file and to `START`/`STOP`/`RESTART` the application in the neede environments.
- Configure you project variable to have a `CRYPT_PASS_FILE` file variable which should contains the **salt and password** to decrypt your configuration secrets. (This is optional if you don’t have any secret).

Notes
-----

The first line commit message should be in the following format for the action to occur:
  - `DEPLOY on environment (comp1=ver1, comp2=ver2, …)`
  - `UNDEPLOY on environment`

Any change to your `deploy.yaml` file will generate such a commit message (using git hooks).

In the `docker-compose.yml` file, each component gets is version filled in a `COMPONENT_VERSION` variable.
For instance for a `back` component, the `BACK_VERSION` variable is available.

See [DCA format](https://github.com/jrd/dca_format) and [DCA example](https://gitlab.com/systra/qeto/infra/dca_example) for more informations.

Usage
-----

Commit and push a `deploy.yaml` file with the content of each component version you want to deploy to a specific environment.

You can also undeploy an application from a specific environment.
