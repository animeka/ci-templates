Goal
====

The objective is to setup a git repository to enable encrypted secrets along with plain files so that they can be kept and managed in version control system.

How to setup your repository
============================

For a not-encrypted repository
------------------------------

- Download the [`config-secret`](https://gitlab.com/animeka/ci-templates/-/raw/master/secret/config-secret) file to your own repository.
- Make the file executable `chmod +x config-secret`
- Add the file to your repository: `git add config-secret`
- Run the `./config-secret` file:
  - provide a 16 digits hex salt (or type `gen` to generate one)
  - provide a strong password (or type `gen` to generate one)
  - tell which directories to encrypt (based on your root repository)
- Add the `.gitattributes` file to your repository: `git add .gitattributes`
- Commit the changes: `git commit`
- Optionnaly, check that the blob is encrypted: `git cat-file -p HEAD:<path-to-file>`
- Publish the changes: `git push`
- Share the **SALT** and **PASSWORD** with your team (or the `.git/crypt.pass` file)

When you clone an encrypted repository
--------------------------------------

- Clone your encrypted repository
- Get the **SALT** and **PASSWORD** from your team members
- Run `./config-secret` file:
  - provide the shared salt
  - provide the shared password
  - answer `y` to the unlock question
- Profit

How to generate Salt
====================

You need to generate a 16 digits hex salt.

Here is one possible way:

```sh
openssl rand -hex 8
```
